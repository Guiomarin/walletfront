import 'dart:async';
import 'package:walletfront/common/platform/platformScaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:walletfront/widgets/login.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final int splashDuration = 4;

  startTime() async {
    return Timer(Duration(seconds: splashDuration), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => Login()));
    });
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    var drawer = Drawer();

    return PlatformScaffold(
        drawer: drawer,
        body: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 80),
                    child: Image.asset("assets/logo_wallet.png")),
                SizedBox(height: 20.0),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    alignment: FractionalOffset(0.5, 0.3),
                    child: Text(
                      "Mi Cartera",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 40.0, color: Colors.deepOrange[800]),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                  child: Text(
                    "UNIAJC 2019-2",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            )));
  }
}
