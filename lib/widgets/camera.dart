import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:walletfront/common/platform/platformScaffold.dart';

class Camera extends StatefulWidget {
  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> {
  var starting = true;
  var indexCamera = 0;
  @override
  Widget build(BuildContext context) {
    if (/*!controller.value.isInitialized*/ starting)
      return Center(
        child: CircularProgressIndicator(),
      );
    else
      return PlatformScaffold(
        appBar: AppBar(
          title: Text("Foto del Movimiento"),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(
                (indexCamera == 0 ? Icons.camera_rear : Icons.camera_front),
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  starting = true;
                  indexCamera = indexCamera == 0 ? 1 : 0;
                  loadDevice();
                });
              },
            )
          ],
        ),
        body: CameraPreview(controller),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera_alt),
          onPressed: () async {
            try {
              final path = join(
                (await getTemporaryDirectory()).path,
                '${DateTime.now()}.png',
              );
              await controller.takePicture(path);
              Navigator.pop(context, path);
            } catch (e) {
              print(e);
            }
          },
        ),
      );
  }

  CameraController controller;
  List<CameraDescription> cameras;

  @override
  void initState() {
    loadDevice();
    super.initState();
  }

  loadDevice() {
    loadCamera().then((value) {
      controller =
          CameraController(cameras[indexCamera], ResolutionPreset.veryHigh);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {
          starting = false;
        });
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  Future<void> loadCamera() async {
    cameras = await availableCameras();
  }
}
