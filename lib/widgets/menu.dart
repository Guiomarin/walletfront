import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:walletfront/common/platform/platformScaffold.dart';
import 'package:walletfront/common/widgets/alertdialog.dart';
import 'package:walletfront/models/movement.dart';
import 'package:walletfront/models/wallet.dart';
import 'package:walletfront/utils/auth.dart';
import 'package:walletfront/utils/config.dart';
import 'package:walletfront/widgets/newmovement.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:walletfront/widgets/newwallet.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:walletfront/widgets/v_detalle.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  var wallet = new List<Wallet>();
  int indexWalletSelected = 0;
  int _selectedIndex = 0;
  List<Widget> _widgetOptions;
  static GoogleMapController mapController;
  static LatLng _center = const LatLng(3.43722, -76.5225);
  var loading = true;
  var currentLocation;
  var location = new Location();
  Set<Marker> _markers = Set();

  static void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    syncInfo();
  }

  @override
  Widget build(BuildContext context) {
    mbIniciacializacion(context);
    return PlatformScaffold(
      appBar: AppBar(
        title: Text(wallet.length > 0
            ? wallet[indexWalletSelected].name
            : "Mi Cartera"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(
              Icons.exit_to_app,
              color: Colors.white,
            ),
            onPressed: () {
              Auth.logout(context);
            },
          )
        ],
        centerTitle: true,
        backgroundColor: Colors.deepOrange[800],
      ),
      body: Center(child: parent()),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet),
            title: Text('Cartera'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text('Mapa'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Gestión'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.deepOrange[800],
        onTap: _onItemTapped,
      ),
    );
  }

  Widget parent() {
    if (loading)
      return CircularProgressIndicator();
    else
      return _widgetOptions.elementAt(_selectedIndex);
  }

  void mbIniciacializacion(BuildContext context) {
    if (wallet.length > 0) {
      List<Movement> items = wallet[indexWalletSelected].movements;
      List<Movement> itemsreversed = items.reversed.toList();
      _markers = Set();
      for (var move in itemsreversed) {
        _markers.add(
          Marker(
            markerId: MarkerId(move.id.toString()),
            infoWindow: InfoWindow(
                title: move.concept,
                snippet: move.concept + ' Valor: ' + move.ammount.toString()),
            position: LatLng(move.latitude, move.longitude),
          ),
        );
      }
    }

    location.getLocation().then((dato) {
      _center = LatLng(dato.latitude, dato.longitude);
    });

    _widgetOptions = <Widget>[tabCartera(), tabMap(), tabGestion()];
  }

  Widget tabMap() {
    if (wallet.length == 0)
      return Center(child: Text("No hay Carteras Existentes"));
    else if (wallet[indexWalletSelected].movements.length == 0)
      return Center(child: Text("No hay Movimientos"));
    else
      return GoogleMap(
        onMapCreated: _onMapCreated,
        markers: _markers,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 13.0,
        ),
      );
  }

  Widget tabCartera() {
    if (wallet.length == 0)
      return Center(child: Text("No hay Carteras Existentes"));
    else
      return Scaffold(
        body: Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.circular(10.0),
                            color: Colors.grey[300],
                          ),
                          padding: EdgeInsets.all(20),
                          margin: EdgeInsets.only(top: 20, bottom: 20),
                          child: Column(
                            children: <Widget>[
                              Text(
                                "Saldo Actual",
                                style: TextStyle(color: Colors.black),
                              ),
                              Text(
                                toCurrencyFormat(wallet.length > 0
                                    ? wallet[indexWalletSelected].ammount
                                    : 0),
                                style: TextStyle(
                                    fontSize: 50.0, color: Colors.black),
                              ),
                            ],
                          )),
                    ]),
                Center(
                    child: Text(
                  "Movimientos",
                  style: TextStyle(fontSize: 20.0, color: Colors.red),
                )),
                Expanded(
                  child: movimientos(),
                ),

                //movimientos()
              ]),
          padding: const EdgeInsets.all(3.0),
          alignment: Alignment.center,
        ),
        floatingActionButton: new FloatingActionButton(
            backgroundColor: Colors.blue,
            child: new Icon(Icons.attach_money),
            onPressed: () => navNewMovement()),
      );
  }

  Widget tabGestion() {
    return PlatformScaffold(
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Text(
                      "Mis Carteras",
                      style: TextStyle(
                          fontSize: 24.0,
                          color: const Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                    ),
                  )
                ]),
            Expanded(
              child: ListView.builder(
                  itemCount: wallet.length,
                  padding: EdgeInsets.all(5),
                  itemBuilder: (context, index) {
                    return cardWallets(index);
                  }),
            )
          ]),
      floatingActionButton: new FloatingActionButton(
          backgroundColor: Colors.blue,
          child: new Icon(Icons.account_balance_wallet),
          onPressed: () => navNewWallet()),
    );
  }

  toCurrencyFormat(int value) {
    return FlutterMoneyFormatter(
            amount: value.toDouble(),
            settings: MoneyFormatterSettings(
                symbol: '\$',
                thousandSeparator: '.',
                decimalSeparator: ',',
                symbolAndNumberSeparator: ' ',
                fractionDigits: 0))
        .output
        .symbolOnLeft;
  }

  syncInfo() async {
    Auth.getToken().then((token) {
      try {
        loading = true;
        http.get(url + "wallets", headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            var responseJson =
                json.decode(response.body).cast<Map<String, dynamic>>();
            setState(() {
              loading = false;
              this.wallet.clear();
              this.wallet = responseJson
                  .map<Wallet>((json) => Wallet.fromJson(json))
                  .toList();
              recommendation();
            });
          } else {
            Auth.logout(context);
          }
        });
      } catch (ex) {
        loading = false;
        Alert.show(context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
        debugPrint('Error' + ex.toString());
      }
    });
  }

  Widget movimientos() {
    List<Movement> items = wallet[indexWalletSelected].movements;
    List<Movement> itemsreversed = items.reversed.toList();
    if (itemsreversed.length > 0)
      return ListView.builder(
          itemCount: itemsreversed.length,
          padding: EdgeInsets.only(top: 10),
          itemBuilder: (context, index) {
            return cardMovimientos(itemsreversed, index);
          });
    else
      return Text("No hay movimientos");
  }

  Widget cardMovimientos(List<Movement> items, int index) {
    return new Container(
        child: InkWell(
      onTap: () {
        showDetails(items[index]);
      },
      child: Card(
        elevation: 5,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Container(
          height: 85.0,
          padding: EdgeInsets.only(top: 5),
          child: Row(
            children: <Widget>[
              Container(
                height: 85.0,
                width: 70.0,
                child: Padding(
                  padding: EdgeInsets.only(left: 2),
                  child: new IconTheme(
                    data: new IconThemeData(
                        color: (items[index].ammount > 0
                            ? Colors.green
                            : Colors.red)),
                    child: new Icon((items[index].ammount < 0
                        ? Icons.arrow_back
                        : Icons.arrow_forward)),
                  ),
                ),
              ),
              Container(
                height: 100,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 260,
                        child: Text(
                          items[index].concept,
                          style:
                              new TextStyle(color: Colors.blue, fontSize: 20.0),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                        child: Container(
                            width: 260,
                            child: Text(
                              items[index].date,
                              style: new TextStyle(color: Colors.green),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 2),
                        child: Container(
                          width: 260,
                          child: Text(
                            toCurrencyFormat(items[index].ammount),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 25,
                                color: Color.fromARGB(255, 48, 48, 54)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }

  navNewWallet() async {
    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => NewWallet()));
    syncInfo();
  }

  navNewMovement() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NewMovement(
                wallet: wallet[indexWalletSelected], coordinates: _center)));
    syncInfo();
  }

  Widget cardWallets(int index) {
    return new Container(
      child: InkWell(
        onTap: () {
          setState(() {
            indexWalletSelected = index;
            Fluttertoast.showToast(
                msg: 'Cartera Seleccionada: ' + wallet[index].name,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                backgroundColor: Colors.purple,
                textColor: Colors.white);
          });
        },
        child: Card(
          elevation: 5,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          color: (index == indexWalletSelected
              ? Colors.blueGrey[100]
              : Colors.white),
          child: Container(
            height: 85.0,
            padding: EdgeInsets.only(top: 5),
            child: Row(
              children: <Widget>[
                Container(
                  height: 85.0,
                  width: 70.0,
                  child: Padding(
                    padding: EdgeInsets.only(left: 2),
                    child: new IconTheme(
                      data: new IconThemeData(color: Colors.purple),
                      child: new Icon(Icons.account_balance_wallet),
                    ),
                  ),
                ),
                Container(
                  height: 100,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 260,
                          child: Text(
                            wallet[index].name,
                            style: new TextStyle(
                                color: Colors.blue, fontSize: 20.0),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                          child: Container(
                              width: 260,
                              child: Text(
                                wallet[index].lastUpdate,
                                style: new TextStyle(color: Colors.green),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 2),
                          child: Container(
                            width: 260,
                            child: Text(
                              toCurrencyFormat(wallet[index].ammount),
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Color.fromARGB(255, 48, 48, 54)),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  showDetails(Movement item) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => DetalleView(item: item)));
  }

  recommendation() {
    List<Movement> itemsreversed =
        wallet[indexWalletSelected].movements.reversed.toList();
    if (wallet.length > 0) if (wallet[indexWalletSelected].movements.length >
        0) if (itemsreversed[0].ammount <
            0 &&
        wallet[indexWalletSelected].ammount < 100000)
      Alert.show(context, "¡Cuida el ahorro!",
          "Recuerda mantener un dinerito en la Cartera");
  }
}
