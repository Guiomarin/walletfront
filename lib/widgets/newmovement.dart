import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:walletfront/common/platform/platformScaffold.dart';
import 'package:walletfront/common/widgets/alertdialog.dart';
import 'package:walletfront/models/movement.dart';
import 'package:walletfront/models/wallet.dart';
import 'package:walletfront/utils/config.dart';
import 'package:walletfront/utils/auth.dart';
import 'package:walletfront/widgets/camera.dart';
import 'package:http/http.dart' as http;

class NewMovement extends StatefulWidget {
  final Wallet wallet;
  final LatLng coordinates;

  @override
  _NewMovementState createState() => _NewMovementState(wallet, coordinates);

  // En el constructor, se requiere el objeto Todo
  NewMovement({Key key, @required this.wallet, this.coordinates})
      : super(key: key);
}

class _NewMovementState extends State<NewMovement> {
  final TextEditingController _montoController = TextEditingController();
  final TextEditingController _conceptoController = TextEditingController();
  bool gastoVal = false;
  BuildContext context;
  Wallet wallet;
  LatLng coordinates;
  Movement move = new Movement();
  var path;

  _NewMovementState(this.wallet, this.coordinates);

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Container(
      child: PlatformScaffold(
          appBar: AppBar(
            title: Text(
              "Nuevo Movimiento",
            ),
            centerTitle: true,
            backgroundColor: Colors.deepOrange[800],
          ),
          backgroundColor: Colors.white,
          body: Container(
            child: Padding(
              padding: EdgeInsets.fromLTRB(45.0, 0.0, 45.0, 0.0),
              child: ListView(children: <Widget>[
                Container(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
                    child: Image.asset("assets/montos.png"),
                  ),
                ),
                TextField(
                  controller: _conceptoController,
                  decoration: InputDecoration(
                      labelText: 'Concepto del Movimiento',
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                SizedBox(height: 12.0),
                TextField(
                  controller: _montoController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: 'Monto del Movimiento',
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                SizedBox(height: 0.0),
                Row(
                  children: <Widget>[
                    Checkbox(
                      value: gastoVal,
                      onChanged: (bool value) {
                        setState(() {
                          gastoVal = value;
                        });
                      },
                    ),
                    Text("Gasto"),
                  ],
                ),
                Center(
                  child: path == null
                      ? Text("No hay Foto")
                      : Image.file(
                          File(path),
                          scale: 8,
                        ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: Container(
                    height: 55.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (_conceptoController.text.length == 0 ||
                            _montoController.text.length == 0) {
                          Alert.show(context, "¡Completar los Campos!",
                              "Recuerda diligenciar los datos solicitados para continuar el registro");
                        } else if (gastoVal &&
                            this.wallet.ammount <
                                int.parse(_montoController.text)) {
                          Alert.show(context, "¡Movimiento Erroneo!",
                              "El Valor del Gasto supera el Saldo Actual de la Cartera");
                        } else {
                          move.concept = _conceptoController.text;
                          move.ammount = int.parse(_montoController.text);
                          move.ammount *= (gastoVal) ? -1 : 1;
                          move.latitude = coordinates.latitude;
                          move.longitude = coordinates.longitude;
                          sendImageBase64();
                          move.wallet = this.wallet;
                          registrarMovimiento(move);
                        }
                      },
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0)),
                      child: Text("Agregar",
                          style:
                              TextStyle(color: Colors.white, fontSize: 22.0)),
                      color: Colors.deepOrange[800],
                    ),
                  ),
                )
              ]),
            ),
          ),
          floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.deepOrange[800],
              child: new Icon(Icons.camera),
              onPressed: () => onShowCamera())),
    );
  }

  sendImageBase64() {
    this.move.image = this.path != null
        ? base64Encode(new File(this.path).readAsBytesSync())
        : "";
  }

  void onShowCamera() async {
    path = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => Camera()));
    if (path != null) setState(() {});
  }

  void registrarMovimiento(Movement movement) {
    Auth.getToken().then((token) {
      try {
        print(url + "movements/create");
        print(json.encode(movement.toJson()));
        http.post(url + "movements/create",
            body: json.encode(movement.toJson()),
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader: "Bearer " + token
            }).then((http.Response response) {
          print(response.toString());
          if (response.statusCode == 200) {
            var responseJson = json.decode(response.body);
            Navigator.pop(this.context, Movement.fromJson(responseJson));
          } else {
            print(response.statusCode);
            Alert.show(this.context, "¡Algo anda MAL!",
                "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
          }
        });
      } catch (ex) {
        Alert.show(this.context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
      }
    });
  }
}
