import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:walletfront/common/platform/platformScaffold.dart';
import 'package:walletfront/common/widgets/alertdialog.dart';
import 'package:walletfront/models/wallet.dart';
import 'package:http/http.dart' as http;
import 'package:walletfront/utils/auth.dart';
import 'package:walletfront/utils/config.dart';

class NewWallet extends StatefulWidget {
  @override
  _NewWalletState createState() => _NewWalletState();
}

class _NewWalletState extends State<NewWallet> {
  final TextEditingController _montoController = TextEditingController();
  final TextEditingController _nombreController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PlatformScaffold(
        appBar: AppBar(
          title: Text(
            "Nueva Cartera",
          ),
          centerTitle: true,
          backgroundColor: Colors.deepOrange[800],
        ),
        backgroundColor: Colors.white,
        body: Container(
          child: Padding(
            padding: EdgeInsets.fromLTRB(45.0, 0.0, 45.0, 0.0),
            child: ListView(children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 40.0),
                  child: Image.asset("assets/cartera.png"),
                ),
              ),
              TextField(
                controller: _nombreController,
                decoration: InputDecoration(
                    labelText: 'Nombre de la Cartera',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _montoController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: 'Monto Inicial',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                child: Container(
                  height: 55.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_nombreController.text.length == 0 ||
                          _montoController.text.length == 0)
                        Alert.show(context, "¡Completar los Campos!",
                            "Recuerda diligenciar los datos solicitados para continuar el registro");
                      else {
                        Wallet w = new Wallet();
                        w.name = _nombreController.text;
                        w.ammount = int.parse(_montoController.text);
                        crearWallet(w);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Text("Crear Cartera",
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    color: Colors.deepOrange[800],
                  ),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  void crearWallet(Wallet wallet) async {
    Auth.getToken().then((token) {
      try {
        http.post(url + "wallets/create",
            body: json.encode(wallet.toJson()),
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader: "Bearer " + token
            }).then((http.Response response) {
          if (response.statusCode == 200) {
            Fluttertoast.showToast(
                msg: 'Cartera Creada Correctamente',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                backgroundColor: Colors.purple,
                textColor: Colors.white);
            Navigator.pop(context);
          } else {
            Alert.show(context, "¡Algo no anda bien!",
                "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
          }
        });
      } catch (ex) {
        Alert.show(context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
      }
    });
  }
}
