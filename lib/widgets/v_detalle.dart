import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:walletfront/models/movement.dart';

class DetalleView extends StatefulWidget {
  final Movement item;
  DetalleView({Key key, @required this.item}) : super(key: key);

  @override
  _DetalleViewState createState() => _DetalleViewState(item);
}

class _DetalleViewState extends State<DetalleView> {
  Movement item;
  _DetalleViewState(this.item);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Detalle del Movimiento'),
        centerTitle: true,
        backgroundColor: Colors.deepOrange[800],
      ),
      body: new Column(children: <Widget>[
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.circular(10.0),
                    color: Colors.grey[300],
                  ),
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 20, bottom: 20),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Valor Movimiento",
                        style: TextStyle(color: Colors.black),
                      ),
                      Text(
                        toCurrencyFormat(item.ammount < 0
                            ? item.ammount * -1
                            : item.ammount),
                        style: TextStyle(fontSize: 50.0, color: Colors.black),
                      ),
                    ],
                  )),
            ]),
        Container(
          margin: EdgeInsets.all(3),
          padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Concepto: ",
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.red,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700)),
                  Text(item.concept,
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontFamily: "Roboto")),
                ],
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Text("Fecha y hora: ",
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.red,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700)),
                  Text(item.date,
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontFamily: "Roboto")),
                ],
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Text("Tipo: ",
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.red,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700)),
                  Text(item.ammount > 0 ? "Ingreso" : "Gasto",
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontFamily: "Roboto")),
                ],
              ),
            ],
          ),
        ),
        Expanded(
            child: Container(
          child: loadImage(),
          margin: EdgeInsets.only(bottom: 10),
        ))
      ]),
    );
  }

  Widget loadImage() {
    if (item.image != "")
      return Image.memory(Base64Decoder().convert(item.image));
    else
      return null;
  }

  toCurrencyFormat(int value) {
    return FlutterMoneyFormatter(
            amount: value.toDouble(),
            settings: MoneyFormatterSettings(
                symbol: '\$',
                thousandSeparator: '.',
                decimalSeparator: ',',
                symbolAndNumberSeparator: ' ',
                fractionDigits: 0))
        .output
        .symbolOnLeft;
  }
}
