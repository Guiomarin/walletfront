import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:walletfront/common/widgets/alertdialog.dart';
import 'package:walletfront/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:walletfront/utils/config.dart';

class RegistroUser extends StatefulWidget {
  @override
  _RegistroUserState createState() => _RegistroUserState();
}

class _RegistroUserState extends State<RegistroUser> {
  final TextEditingController _nombresController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: mainUI(context),
    );
  }

  Scaffold mainUI(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Nuevo Usuario",
        ),
        centerTitle: true,
        backgroundColor: Colors.deepOrange[800],
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(45.0, 0.0, 45.0, 0.0),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 40.0),
                  child: Image.asset("assets/cliente.png"),
                ),
              ),
              TextField(
                controller: _nombresController,
                decoration: InputDecoration(
                    labelText: 'Nombre Completo',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _usernameController,
                decoration: InputDecoration(
                    labelText: 'Nombre de Usuario',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _passwordController,
                decoration: InputDecoration(
                    labelText: 'Contraseña',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
                obscureText: true,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                child: Container(
                  height: 55.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_nombresController.text.length == 0 ||
                          _usernameController.text.length == 0 ||
                          _passwordController.text.length == 0)
                        Alert.show(context, "¡Completar los Campos!",
                            "Recuerda diligenciar los datos solicitados para continuar el registro");
                      else {
                        User c = new User();
                        c.name = _nombresController.text;
                        c.username = _usernameController.text;
                        c.password = _passwordController.text;
                        registrarUser(c);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Text("Registrarme",
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    color: Colors.deepOrange[800],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void registrarUser(User user) async {
    try {
      await http
          .post(url + "register", body: json.encode(user.toJson()), headers: {
        HttpHeaders.contentTypeHeader: "application/json",
      }).then((http.Response response) {
        if (response.statusCode == 200) {
          Fluttertoast.showToast(
              msg:
                  'Registro éxitoso, por favor ingresa con la información registrada',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white);
          Navigator.pop(context);
        } else {
          Alert.show(context, "¡Algo no anda bien!",
              "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
        }
      });
    } catch (ex) {
      Alert.show(context, "¡Algo no anda bien!",
          "No es posible la conexión, intenta de nuevo más tarde");
    }
  }
}
