import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:walletfront/common/widgets/alertdialog.dart';
import 'package:walletfront/utils/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:walletfront/widgets/login.dart';
import 'package:walletfront/widgets/menu.dart';

class Auth {
  static void login(BuildContext context, String name, String password) async {
    Map<String, String> body = {
      'name': name,
      'password': password,
    };
    try {
      await http.post(url + "login", body: json.encode(body), headers: {
        HttpHeaders.contentTypeHeader: "application/json"
      }).then((http.Response response) {
        var responseJson = json.decode(response.body);
        if (response.statusCode == 200) {
          //var user = new User.fromJson(responseJson);
          var user = responseJson["jwtKey"];
          saveCurrentLogin(user);
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Menu()));
        } else {
          Alert.show(context, "¡Algo no anda bien!",
              "La combinación de credenciales suministradas, no son correctas. Por favor verifica nuevamente");
        }
      });
    } catch (ex) {
      Alert.show(context, "¡Algo no anda bien!",
          "No es posible la conexión, intenta de nuevo más tarde");
    }
  }

  static Future<String> getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString("token");
  }

  static Future<int> getId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt("id");
  }

  static saveCurrentLogin(/*Space user*/ token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    //await preferences.setString('token', user.token);
    //await preferences.setInt('id', user.id);
    await preferences.setString('token', token);
  }

  static logout(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Login()));
  }
}
