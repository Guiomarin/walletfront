class Recommendation {
  String concept;

  Recommendation();

  Recommendation.fromJson(Map<String, dynamic> json)
      : concept = json['concept'];        

  Map<String, dynamic> toJson() => {
      'concept': concept
      };
}