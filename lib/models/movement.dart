
import 'package:walletfront/models/wallet.dart';

class Movement {
  int id;
  String concept;
  String date;
  int ammount;
  String image;
  double latitude;
  double longitude;
  Wallet wallet;

  Movement();

  Movement.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        concept = json['concept'],
        date = json['date'],
        ammount =  json['ammount'],
        image = json['image'],
        latitude = json['latitude'],
        longitude = json['longitude'];
        

  Map<String, dynamic> toJson() => {
        'concept': concept,
        'ammount': ammount,
        'image': image,
        'latitude': latitude,
        'longitude': longitude,
        'wallet' : wallet.toJson()
      };
}