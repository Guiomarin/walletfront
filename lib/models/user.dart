class User {
  int id;
  String username;
  String name;
  String token;
  String password;

  User();

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        username = json['username'],
        name = json['name'],
        token =  json['jwt'];
        

  Map<String, dynamic> toJson() => {
        'username': username,
        'password': password,
        'name': name
      };
}