import 'package:walletfront/models/movement.dart';

class Wallet {
  int id;
  String name;
  String lastUpdate;
  int ammount;
  List<Movement> movements;

  Wallet();

  Wallet.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        lastUpdate = json['lastupdate'],
        ammount = json['ammount'],
        movements = new List<Movement>.from(json['movements']
            .map<Movement>((json) => Movement.fromJson(json))
            .toList());

  Map<String, dynamic> toJson() => {'name': name, 'ammount': ammount, 'id': id};
}
